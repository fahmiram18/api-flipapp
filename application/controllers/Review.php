<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Review extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('review_model','review');
    }

    public function index_get()
    { //mengambil data review
        $id = $this->get('app_id');
        if ($id != "") {
            $review = $this->review->get($id);
        } else {
            $review = $this->review->get();
        }
        
        if ($review) {
            $this->response([
                'status' => true,
                'data' => $review
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'data' => [],
                'message' => 'id not found' 
            ], REST_Controller::HTTP_OK);
        }
    }

    public function count_get()
    {
        $id = $this->get('app_id');
        $count = $this->review->count($id);
        if ($count) {
            $this->response([
                'status' => true,
                'data' => $count
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => true,
                'data' => '0' 
            ], REST_Controller::HTTP_OK);
        }
    }

    public function average_get()
    {
        $app_id = $this->get('app_id');
        $avg = $this->review->average($app_id);
        // $avg = (array_sum($avg) / count($avg));
        // $hasil = 0;
        // for ($i=0; $i < count($avg); $i++) { 
        //     $hasil += $avg[$i]['review_rating'];
        // }

        if ($avg != null) {
            $this->response([
                'status' => true,
                'data' => $avg[0]
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'product not found' 
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_delete()
    { //menghapus review
        $id = $this->delete('review_id');
        if ($id == null) {
            $this->response([
                'status' => false,
                'message' => 'provide an id!' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->review->delete($id)>0) {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message'=> 'deleted.'
                ], REST_Controller::HTTP_OK);
            } else {
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'id not found!' 
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_post()
    { //menambah review

        $data = $this->post('data');

        if ($this->review->add($data) > 0) {
            $this->response([
                'status' => true,
                'message'=> 'new Info has been created'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed to created new Info!' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    { 
        $data = $this->put('data');
        $id = $data['review_id'];
        
        if ($this->review->update($data,$id) > 0) {
            $this->response([
                'status' => true,
                'message'=> 'info has been updated'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed to update info!' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}

/* End of file Review.php */
/* Location: ./application/controllers/api/Review.php */