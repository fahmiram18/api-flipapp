<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Order extends REST_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('order_model','order');
    }

    public function index_get()
    { 
        $id = $this->get('checkout_token');
        $keyword = $this->get('keyword');

        if ($id != "") {
            $checkout = $this->order->get($id); 
        } else if ($keyword != "") {
        	$checkout = $this->order->search($keyword);
        } else {
            $checkout = $this->order->get();
        }
        
        if ($checkout) {
            $this->response([
                'status' => true,
                'data' => $checkout
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'id not found' 
            ]);
        }
    }

	public function index_delete(){ //menghapus category
        $id = $this->delete('checkout_token');
        if ($id == null) {
            $this->response([
                'status' => false,
                'message' => 'Invalid Token' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->order->finish($id) == 0) {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message'=> 'deleted'
                ], REST_Controller::HTTP_OK);
            } else {
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'id not found!' 
                ]);
            }
        }
    }

}

/* End of file Order.php */
/* Location: ./application/controllers/api/Order.php */