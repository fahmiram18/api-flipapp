<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Portfolio extends REST_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('portfolio_model', 'portfolio');

	}

	public function index_get()
	{
		$user_id = $this->get('user_id');
		$portfolio = $this->portfolio->get($user_id);		

		if ($portfolio) {
			$this->response([
				'status' => true,
				'data' => $portfolio

			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => false,
				'message' => 'id not found',
				'data' => $portfolio
			]);
		}
	} 
}


