<?php 
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Auth extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model','auth');
    }
    // fungsi login
    public function login_post()
    {
        $email = $this->post('user_email');
        $password = $this->post('user_password');
        $auth = $this->auth->get($email);

        // validasi email
        if ($auth){
            // validasi password
            if (password_verify($password, $auth['user_password'])) {
                $data = [
                    'user_id' => $auth['user_id'],
                    'user_name' => $auth['user_name'],
                    'user_email' => $auth['user_email'],
                    'user_phone' => $auth['user_phone'],
                    'user_avatar' => $auth['user_avatar']
                ];

                $this->response([
                    'status' => true,
                    'data' => $data
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Invalid password!'
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Invalid e-mail!'
            ]);
        }
    }

    // fungsi registrasi via email
    public function signup_post()
    {
        $data = [
            'user_name' => $this->post('user_name'),
            'user_email' => $this->post('user_email'),
            'user_phone' => $this->post('user_phone'),
            'user_joined' => date("Y-m-d"),
            'user_password' => password_hash($this->post('user_password'), PASSWORD_DEFAULT),
            'user_active_status' => 1
        ];

        // var_dump($data);

        if ($this->auth->get($this->post('user_email'))) {
            $this->response([
                'status' => false,
                'message' => 'E-mail has been registered!'
            ]);
        } else {
            if ($this->auth->create($data) > 0) {
                $this->response([
                    'status' => true,
                    'message'=> 'Sign Up Successfull!'
                ], REST_Controller::HTTP_CREATED);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Sign Up Unsuccessfull! Try Again later.' 
                ]);
            }
        }
    }

    // belum
    public function google_post()
    {
        $data = [
            // 'id'=> $this->post('id'),
            'name'=> $this->post('user_name'),
            'email'=> $this->post('user_email'),
            'password'=> $this->post('user_password'),
            'phone'=>$this->post('user_phone'),
        ];

        $auth = $this->auth->get($data['user_email']);

        if ($auth) {

            $user = [
                'id' => $auth['user_id'],
                'name' => $auth['user_name'],
                'email' => $auth['user_email'],
                'phone' => $auth['user_phone'],
                'avatar' => $auth['user_avatar']
            ];

            $this->response([
                'status' => true,
                'data' => $user
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => "Email not registered"
            ], REST_Controller::HTTP_OK);
        }
    }
}

/* End of file Auth.php */
/* Location: ./application/controllers/api/Auth.php */