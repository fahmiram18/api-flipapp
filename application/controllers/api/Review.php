<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Review extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('review_model','review');
    }

    public function index_get()
    { //mengambil data review
        $app_id = $this->get('app_id');
        if ($app_id != null) {
            $review = $this->review->get($app_id);
            if ($review) {
                $this->response([
                    'status' => true,
                    'total_review' => count($review),
                    'data' => $review
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'total_review' => count($review),
                    'message' => 'No reviews!'
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        }
    }

    // public function count_get()
    // {
    //     $id = $this->get('app_id');
    //     $count = $this->review->count($id);
    //     if ($count) {
    //         $this->response([
    //             'status' => true,
    //             'data' => $count
    //         ], REST_Controller::HTTP_OK);
    //     } else {
    //         $this->response([
    //             'status' => true,
    //             'data' => '0' 
    //         ], REST_Controller::HTTP_OK);
    //     }
    // }

    public function index_delete()
    {
        $review_id = $this->delete('review_id');
        $app_id = $this->delete('app_id');
        if ($review_id != null) {
            $review = $this->review->delete($review_id);
            if ($review) {
                $this->review->_average($app_id);
                $this->response([
                    'status' => true,
                    'message' => 'Review deleted!'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Failed to delete a review!' 
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        }        
    }

    // public function index_delete()
    // { //menghapus review
    //     $id = $this->delete('review_id');
    //     if ($id == null) {
    //         $this->response([
    //             'status' => false,
    //             'message' => 'provide an id!' 
    //         ], REST_Controller::HTTP_BAD_REQUEST);
    //     } else {
    //         if ($this->review->delete($id)>0) {
    //             $this->response([
    //                 'status' => true,
    //                 'id' => $id,
    //                 'message'=> 'deleted.'
    //             ], REST_Controller::HTTP_OK);
    //         } else {
    //             //id not found
    //             $this->response([
    //                 'status' => false,
    //                 'message' => 'id not found!' 
    //             ], REST_Controller::HTTP_BAD_REQUEST);
    //         }
    //     }
    // }

    public function index_post()
    { //menambah review
        $app_id = $this->post('app_id');
        $data = [
            'app_id' => $app_id,
            'user_id' => $this->post('user_id'),
            'review_date' => $this->post('review_date'),
            'review_desc' => $this->post('review_desc'),
            'review_rating' => $this->post('review_rating'),
            'review_like' => $this->post('review_like')
        ];
        $review = $this->review->create($data);
        if ($review) {
            $this->review->_average($app_id);
            $this->response([
                'status' => true,
                'message'=> 'Review posted!'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to post a review!' 
            ]);
        }
    }

    public function index_put()
    { 
        $review_id = $this->put('review_id');
        $app_id = $this->put('app_id');
        $data = [
            'app_id' => $app_id,
            'user_id' => $this->put('user_id'),
            'review_date' => date("Y-m-d"),
            'review_desc' => $this->put('review_desc'),
            'review_rating' => $this->put('review_rating'),
            'review_like' => $this->put('review_like')
        ];
        if ($review_id != null) {
            $review = $this->review->update($data, $review_id);
            if ($review) {
                $this->review->_average($app_id);
                $this->response([
                    'status' => true,
                    'message'=> 'Review updated!'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Failed to update a review!' 
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        }
    }
}

/* End of file Review.php */
/* Location: ./application/controllers/api/Review.php */