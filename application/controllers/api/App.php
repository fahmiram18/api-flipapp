<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class App extends REST_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('app_model','app');
    }

    public function index_get()
    {
        $keyword = $this->get('keyword');
        $category_id = $this->get('category_id');
        $app_id = $this->get('app_id');

        if ($keyword != "") {
        	$app = $this->app->search($keyword);
        } else if ($category_id != null) {
        	$app = $this->app->filter($category_id);
        } else if ($app_id != "") {
            $app = $this->app->get($app_id);
        } else {
        	$app = $this->app->get();
        }       

        if ($app) {
            $this->response([
                'status' => true,
                'data' => $app
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data not found'
            ]);
        }
    }

    public function index_delete()
    {
        $app_id = $this->delete('app_id');
        if ($app_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!'
            ]);
        } else {
            $app = $this->app->delete($app_id);

            if ($app) {
                $this->response([
                    'status' => true,
                    'data' => $app_id
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false
                ]);
            }
        }
    }

    public function index_post()
    { //menambah App
        $image = file_get_contents($this->post("app_poster"));
		$image_name = 'img_' . time();
		$filename = $image_name . '.' . 'jpg';
		$path = "assets/img/poster/";
		file_put_contents($path . $filename, $image);

		$data = [
            'app_poster' => base_url() . 'assets/img/poster/' . $filename,
            "category_id" => $this->post('category_id'),
            "user_id" => $this->post('user_id'),
            "app_name" => $this->post('app_name'),
            "app_desc" => $this->post('app_desc'),
            "app_price" => $this->post('app_price'),
            "app_screen_capture_1" => base_url()."/assets/img/screen_capture/default.jpg",
            "app_screen_capture_2" => base_url()."/assets/img/screen_capture/default.jpg",
            "app_screen_capture_3" => base_url()."/assets/img/screen_capture/default.jpg",
            "app_version" => $this->post('app_version'),
            "app_released" => $this->post('app_released'),
            "app_review_value" => 0,
            "app_requirement" => $this->post('app_requirement'),
            'app_demo_link' => $this->post('app_demo_link'),
            "app_last_updated" => date("Y-m-d"),
            "app_status_id" => 2,
            "app_purchased" => 0
        ];
        
        $app = $this->app->create($data);

        if ($app) {
            $this->response([
                'status' => true,
                'data'=>$data
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to post an app!',
            ]);
        }

    }

    public function update_post()
    {
        $app_id = $this->post('app_id');
        
        
        $data = [
            "category_id" => $this->post('category_id'),
            "user_id" => $this->post('user_id'),
            "app_name" => $this->post('app_name'),
            "app_desc" => $this->post('app_desc'),
            "app_price" => $this->post('app_price'),
            "app_poster" => $this->post('old_poster'),
            "app_screen_capture_1" => base_url()."assets/img/screen_capture/default.jpg",
            "app_screen_capture_2" => base_url()."assets/img/screen_capture/default.jpg",
            "app_screen_capture_3" => base_url()."assets/img/screen_capture/default.jpg",
            "app_version" => $this->post('app_version'),
            // "app_released" => $this->post('app_released'),
            // "app_review_value" => $this->post('app_review_value'),
            "app_requirement" => $this->post('app_requirement'),
            'app_demo_link' => $this->post('app_demo_link'),
            "app_last_updated" => date("Y-m-d"),
            "app_status_id" => 2,   
            // "app_purchased" => $this->post('app_purcashed'),
        ];
        
        if ($this->post('app_poster') != null) {
            $image = file_get_contents($this->post("app_poster"));
            $image_name = 'img_' . time();
            $filename = $image_name . '.' . 'jpg';
            $path = "assets/img/poster/";
            file_put_contents($path . $filename, $image);
            $data['app_poster'] = base_url() . $path . $filename;
        }
        
        $app = $this->app->update($app_id, $data);

        if ($app) {
            $this->response([
                'status' => true,
                'data'=>$data
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to update an app!',
            ]);
        }

        // $config['file_name'] = 'img_'.time();
        // $config['upload_path'] = 'assets/img/poster/';
        // $config['allowed_types'] = 'jpeg|jpg|png';
        // $config['max_size'] = 1024;
        // $config['max_width'] = 1024;
        // $config['max_height'] = 768;
        // $this->load->library('upload', $config);

        // if ($this->upload->do_upload('app_poster')) {
        //     $data['app_foto'] = base_url() . 'assets/img/poster/' . $this->upload->data('file_name');
        // }

        // if ($this->app->update($app_id, $data) > 0) {
        //     $this->response([
        //         'status' => true,
        //         'data'=>$data
        //     ], REST_Controller::HTTP_OK);
        // } else {
        //     $this->response([
        //         'status' => false
        //     ]);
        // }

    }
}

/* End of file app.php */
/* Location: ./application/controllers/api/app.php */