<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Transaction extends REST_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('transaction_model','transaction');
    }

    public function index_get()
    {
        $user_id = $this->get('user_id');
        $transaction = $this->transaction->get($user_id);

        if ($transaction) {
            $this->response([
                'status' => true,
                'message'=> 'Transaction has been retrieved!',
                'data' => $transaction
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to retrieved transaction!'
            ]);
        }
    }

    // public function detail_get()
    // {
    //     $token = $this->get('checkout_token');
    //     $transaction = $this->transaction->getDetail($token);

    //     if ($transaction) {
    //         $this->response([
    //             'status' => true,
    //             'message'=> 'Transaction has been retrieved!',
    //             'data' => $transaction
    //         ], REST_Controller::HTTP_OK);
    //     } else {
    //         $this->response([
    //             'status' => false,
    //             'message' => 'Failed to retrieved transaction!'
    //         ]);
    //     }
    // }

    public function index_post()
    {
        $token = $this->_getToken(9);
        $user_id = $this->post('user_id');
        $app_id = $this->post('app_id');
        $history = [];
        
        for ($i=0; $i < count($app_id); $i++) { 
            $array = [
                'user_id' => $user_id,
                'app_id' => $app_id[$i],
                'checkout_token' => $token
            ];
            array_push($history, $array);
        }

    	$data = [
    		'checkout_token' => $token,
            'user_id' => $user_id,
            'shipping_method_id' => $this->post('shipping_method_id'),
            'shipping_date' => $this->post('shipping_date'),
            'comment' => $this->post('comment'),
            'total_price' => $this->post('total_price'),
    		'transaction_status_id' => 1, // waiting for payment confirmation
    		'last_updated' => date("Y-m-d")
    	];
        
        // var_dump($data);
        // var_dump($history);
        // die;

    	$transaction = $this->transaction->create($data);
        if ($transaction) {
            $this->transaction->_checkedout($user_id);
            $this->transaction->_addHistory($history);
            $this->response([
                'status' => true,
                'message'=> 'Transaction on process!'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to create new transaction!' 
            ]);
        }
    }

    private function _crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    private function _getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited
    
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->_crypto_rand_secure(0, $max-1)];
        }
    
        return $token;
    }

}

/* End of file Transaction.php */
/* Location: ./application/controllers/api/Transaction.php */