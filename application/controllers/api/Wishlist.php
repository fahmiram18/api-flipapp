<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Wishlist extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Wishlist_model', 'wishlist');
	}

	public function index_get()
	{
		$user_id = $this->get('user_id');

		if ($user_id != null) {
			$wishlist = $this->wishlist->get($user_id);
		} else {
			$wishlist = FALSE;
		}

		if ($wishlist) {
			$this->response([
				'status' => TRUE,
				'data' => $wishlist
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Wishlist is empty!'
			]);
		}
	}

	// public function getAll_get()
	// {
	// 	$wishlist = $this->wishlist->getAll();

	// 	if ($wishlist) {
	// 		$this->response([
	// 			'status' => TRUE,
	// 			'data' => $wishlist
	// 		], REST_Controller::HTTP_OK);
	// 	} else {
	// 		$this->response([
	// 			'status' => FALSE,
	// 			'message' => 'Wishlist is empty!'
	// 		]);
	// 	}
	// }

	public function index_post()
	{
		$data = [
			'app_id' => $this->post('app_id'),
			'user_id' => $this->post('user_id')
		];

		$wishlist = $this->wishlist->create($data);

		if ($wishlist) {
			$this->response([
				'status' => TRUE,
				'data' => $wishlist
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Failed to add an app!'
			]);
		}
	}

	public function index_delete()
	{
		$wishlist_id = $this->delete('wishlist_id');

		if ($wishlist_id != null) {
			$wishlist = $this->wishlist->delete($wishlist_id);

			if ($wishlist) {
				$this->response([
					'status' => TRUE,
					'data' => $wishlist_id
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'Failed to remove an app!'
				]);
			}
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Provide an ID'
			]);
		}
	}

}

/* End of file Wishlist.php */
/* Location: ./application/controllers/api/Wishlist.php */