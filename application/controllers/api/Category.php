<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Category extends REST_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('category_model','category');
    }

    //  mengambil data category
    public function index_get()
    {
        $category_id = $this->get('id');
        if ($category_id != null) {
            $category = $this->category->get($category_id);
        } else {
            $category = $this->category->get();
        }
        
        if ($category) {
            $this->response([
                'status' => true,
                'data' => $category
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
            ]);
        }
    }

    //menambah category
    public function index_post()
    { 
        $data = [
            'category_name' => $this->post('category_name')
        ];

        if ($this->category->add($data) > 0) {
            $this->response([
                'status' => true,
                'data'=>$data
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false ,
                'message' => 'Failed to add a category!'
            ]);
        }
    }

    //menghapus category
    public function index_delete()
    {
        $category_id = $this->delete('id');
        if ($category_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Provide an id!' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->category->delete($category_id)>0) {
                $this->response([
                    'status' => true,
                    'data' => $category_id
                ], REST_Controller::HTTP_OK);
            } else {
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'Failed to remove a category!'
                ]);
            }
        }
    }

     //mengedit category
    public function index_put()
    {
        $category_id = $this->put('category_id');
        $data = [
            'category_name' => $this->put('category_name')
        ];

        if ($this->category->update($category_id, $data) > 0) {
            $this->response([
                'status' => true,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to update a category!'
            ]);
        }
    }

}
/* End of file Category.php */
/* Location: ./application/controllers/api/Category.php */