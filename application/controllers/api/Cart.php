<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Cart extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('cart_model', 'cart');
	}

	public function index_get()
	{
		$user_id = $this->get('user_id');

		if ($user_id != null) {
			$cart = $this->cart->get($user_id);
		} else {
			$cart = FALSE;
		}

		if ($cart) {
			$this->response([
				'status' => TRUE,
				'total_price' => $this->cart->_total_price($user_id),
				'data' => $cart
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Cart is empty!'
			]);
		}
	}

	// public function getAll_get()
	// {
	// 	$cart = $this->cart->getAll();

	// 	if ($cart) {
	// 		$this->response([
	// 			'status' => TRUE,
	// 			'data' => $cart
	// 		], REST_Controller::HTTP_OK);
	// 	} else {
	// 		$this->response([
	// 			'status' => FALSE,
	// 		], REST_Controller::HTTP_BAD_REQUEST);
	// 	}
	// }

	public function index_post()
	{
		$data = [
			'app_id' => $this->post('app_id'),
			'user_id' => $this->post('user_id')
		];

		$cart = $this->cart->create($data);

		if ($cart) {
			$this->response([
				'status' => TRUE,
				'data' => $cart
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Failed to add an app!'
			]);
		}
	}

	public function index_delete()
	{
		$cart_id = $this->delete('cart_id');

		if ($cart_id != null) {
			$cart = $this->cart->delete($cart_id);

			if ($cart) {
				$this->response([
					'status' => TRUE,
					'data' => $cart_id
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'Failed to remove an app!'
				]);
			}
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'Provide an ID'
			]);
		}
	}

}

/* End of file cart.php */
/* Location: ./application/controllers/api/cart.php */