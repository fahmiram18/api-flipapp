<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Comment extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('comment_model','comment');
        $this->load->model('reply_model', 'reply');
        
    }

    //mengambil data Comment
    public function index_get()
    { 
        $app_id = $this->get('app_id');
        if ($app_id != null) {
            $comment = $this->comment->get($app_id);

            if ($comment) {
                $this->response([
                    'status' => true,
                    'total_comment' => count($comment),
                    'data' => $comment
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'total_comment' => count($comment),
                    'message' => 'Failed to get comment data!' 
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        }
    }

    //menambah Comment
    public function index_post()
    { 
        $data = [
            'app_id' => $this->post('app_id'),
            'user_id' => $this->post('user_id'),
            'comment_date' => date("Y-m-d"),
            'comment' => $this->post('comment')
        ];

        if ($this->comment->create($data)) {
            $this->response([
                'status' => true,
                'message'=> 'Comment posted!',
                'data'=>$data
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to post a comment!' 
            ]);
        }
    }

     //mengedit comments
    public function index_put()
    {
        $comment_id = $this->put('comment_id');
        $data = [
            'comment' => $this->put('comment'),
            'comment_date' => date("Y-m-d")
        ];

        if ($this->comment->update($comment_id, $data)) {
            $this->response([
                'status' => true,
                'message'=> 'Comment updated!'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to update a comment!' 
            ]);
        }
    }

    public function index_delete()
    {
        $comment_id = $this->delete('comment_id');
        if ($comment_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        } else {
            if ($this->comment->delete($comment_id)) {
                // menghapus semua reply pada comment ketika comment berhasil dihapus
                $this->reply->deleteAll($comment_id);
                $this->response([
                    'status' => true,
                    'message'=> 'Comment deleted!'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Failed to delete a comment!' 
                ]);
            }
        }
    }

}

/* End of file Comment.php */
/* Location: ./application/controllers/api/Comment.php */