<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Account extends REST_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Account_model','account');
	}

	public function index_get()
	{
		$user_id = $this->get('user_id');

		if ($user_id != null) {
			$account = $this->account->get($user_id);
			if ($account) {
				$this->response([
		            'status' => true, 
		            'data' => $account
		        ], REST_Controller::HTTP_OK);
			} else{
				$this->response([
					'status' => false,
					'message' => 'Failed to get user data!'
		        ]);
			}
		} else {
			$this->response([
	            'status' => false,
	            'message' => 'Provide an ID!'
	        ]);
		}
		
	}

	public function personal_put()
	{
		$user_id = $this->put('user_id');
        $data = [
        	'user_name' => $this->put('user_name'),
        	'user_github' => $this->put('user_github'),
        	'user_email' => $this->put('user_email'),
        	'user_phone' => $this->put('user_phone'),
        	'user_company' => $this->put('user_company'),
			'user_company_address' => $this->put('user_company_address'),
			'user_city'=>$this->put('user_city'),
			'user_state'=>$this->put('user_state'),
			'user_zip'=>$this->put('user_zip'),
        	'user_credit' => $this->put('user_credit')
        ];

        if ($user_id != null) {
        	$account = $this->account->update($user_id, $data);
	        if ($account) {
				$this->response([
		            'status' => true,
		            'message' => 'User data has been modified'
		        ], REST_Controller::HTTP_OK );
			} else{
				$this->response([
		            'status' => false,
		            'message' => 'failed to update user data'
		        ]);
			}
        } else {
        	$this->response([
	            'status' => false,
	            'message' => 'Provide an ID!'
	        ]);
        }
	}

	public function credit_put()
	{
		$user_id = $this->put('user_id');
		$data = [
			'user_credit' => $this->put('credit')
		];

		if ($user_id != null) {
        	$account = $this->account->update($user_id, $data);
	        if ($account) {
				$this->response([
		            'status' => true,
		            'message' => 'User data has been modified'
		        ], REST_Controller::HTTP_OK );
			} else{
				$this->response([
		            'status' => false,
		            'message' => 'failed to update user data'
		        ]);
			}
        } else {
        	$this->response([
	            'status' => false,
	            'message' => 'Provide an ID!'
	        ]);
        }
	}

	public function avatar_put()
	{
		$user_id = $this->put('user_id');

		$config['file_name'] = 'img_'.time();
        $config['upload_path'] = 'assets/img/avatar/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        // $config['max_size'] = 6000; // max size == +- 5MB
        // $config['max_width'] = 1024;
        // $config['max_height'] = 768;
        $this->load->library('upload', $config);

        if ($user_id != null) {
        	if ($this->upload->do_upload('user_avatar')) {

		    	$data = [
		    		'user_avatar' => base_url() . 'assets/img/avatar/' . $this->upload->data('file_name')
		    	];

		    	$user = $this->account->update($user_id, $data);

		    	if ($user) {
		            $this->response([
		                'status' => true,
		                'message' => 'User data has been modified!',
		                'data'=>$data
		            ], REST_Controller::HTTP_CREATED);
		        } else {
		            $this->response([
			            'status' => false,
			            'message' => 'Failed to update user data!'
			        ]);
		        }
		    } else {
		    	$this->response([
		            'status' => false,
		            'message' => $this->upload->display_errors(),
		        ]);
		    }
        } else {
        	$this->response([
	            'status' => false,
	            'message' => 'Provide an ID!'
	        ]);
        }  
	}

	public function avatar_post()
	{
		$user_id = $this->post('user_id');

		$image = file_get_contents($this->post("user_avatar"));
		$image_name = 'img_' . time();
		$filename = $image_name . '.' . 'jpg';
		$path = "assets/img/avatar/";
		file_put_contents($path . $filename, $image);

		$data = [
			'user_avatar' => base_url() . 'assets/img/avatar/' . $filename
		];

		$user = $this->account->update($user_id, $data);

		if ($user) {
			$this->response([
				'status' => true,
				'message' => 'User data has been modified!',
				'data'=>$data
			], REST_Controller::HTTP_CREATED);
		} else {
			$this->response([
				'status' => false,
				'message' => $this->upload->display_errors(),
				'message' => 'Failed to update user data!'
			]);
		}

		// $config['file_name'] = 'img_'.time();
        // $config['upload_path'] = 'assets/img/avatar/';
        // $config['allowed_types'] = 'jpeg|jpg|png';
        // $config['max_size'] = 6000; // max size == +- 5MB
        // // $config['max_width'] = 1024;
        // // $config['max_height'] = 768;
        // $this->load->library('upload', $config);

        // if ($user_id != null) {
        // 	if ($this->upload->do_upload('user_avatar')) {

		//     	$data = [
		//     		'user_avatar' => base_url() . 'assets/img/avatar/' . $this->upload->data('file_name')
		//     	];

		//     	$user = $this->account->update($user_id, $data);

		//     	if ($user) {
		//             $this->response([
		//                 'status' => true,
		//                 'message' => 'User data has been modified!',
		//                 'data'=>$data
		//             ], REST_Controller::HTTP_CREATED);
		//         } else {
		//             $this->response([
		// 				'status' => false,
		// 				'message' => $this->upload->display_errors(),
		// 	            'message' => 'Failed to update user data!'
		// 	        ]);
		//         }
		//     } else {
		//     	$this->response([
		//             'status' => false,
		//             'message' => $this->upload->display_errors(),
		//         ]);
		//     }
        // } else {
        // 	$this->response([
	    //         'status' => false,
	    //         'message' => 'Provide an ID!'
	    //     ]);
        // }  
	}

}

/* End of file Account.php */
/* Location: ./application/controllers/api/Account.php */