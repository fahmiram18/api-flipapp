<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Reply extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reply_model','reply');
    }

    public function index_get()
    { //mengambil data Reply    
        $comment_id = $this->get('comment_id');
        if ($comment_id != null) {
            $reply = $this->reply->get($comment_id);

            if ($reply) {
                $this->response([
                    'status' => true,
                    'total_reply' => count($reply),
                    'data' => $reply
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'total_reply' => count($reply),
                    'message' => 'Failed to get reply data!' 
                ]);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        }
    }

    public function index_post()
    {
        $data = [
            'comment_id' => $this->post('comment_id'),
            'user_id' => $this->post('user_id'),
            'reply_date' => date("Y-m-d"),
            'reply' => $this->post('reply')
        ];
        if ($this->reply->create($data)) {
            $this->response([
                'status' => true,
                'message'=> 'Reply posted!'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to post a reply!' 
            ]);
        }
    }

    public function index_delete()
    { //menghapus review
        $reply_id = $this->delete('reply_id');

        if ($reply_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Provide an ID!' 
            ]);
        } else {
            if ($this->reply->delete($reply_id) ) {
                $this->response([
                    'status' => true,
                    'message'=> 'Reply deleted!'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Failed to delete a reply!' 
                ]);
            }
        }
    }

    public function index_put()
    {
        $reply_id = $this->put('reply_id');
        $data = [
            'reply' => $this->put('reply'),
            'reply_date' => date("Y-m-d")
        ];

        if ($this->reply->update($reply_id, $data)) {
            $this->response([
                'status' => true,
                'message'=> 'Reply updated!'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to update a reply!' 
            ]);
        }
    }

}

/* End of file Reply.php */
/* Location: ./application/controllers/api/Reply.php */