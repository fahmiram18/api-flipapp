<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


    class info_c extends REST_Controller {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('info_model','info');
        }

        //menampilkan data
        public function index_get()
        { 
            $id = $this->get('info_id');
            if ($id === null) {
                $info = $this->info->getInfo();
            } else {
                $info = $this->info->getInfo($id);
            }
            
            if ($info) {
                $this->response([
                    'status' => true,
                    'data' => $info
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'id not found' 
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }

        public function index_delete()
        { //menghapus data
            $id = $this->delete('info_id');
            if ($id == null) {
                $this->response([
                    'status' => false,
                    'message' => 'provide an id!' 
                ], REST_Controller::HTTP_BAD_REQUEST);
            } else {
                if ($this->info->deleteInfo($id)>0) {
                    $this->response([
                        'status' => true,
                        'id' => $id,
                        'message'=> 'deleted.'
                    ], REST_Controller::HTTP_OK);
                } else {
                    //id not found
                    $this->response([
                        'status' => false,
                        'message' => 'id not found!' 
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function index_post()
        { //menambah data info
            $data = [
                'app_id'=> $this->post('app_id'),
                'info_version'=> $this->post('info_version'),
                'info_requirement'=> $this->post('info_requirement'),
                'info_last_update'=> $this->post('info_last_update'),
                'info_downloaded'=> $this->post('info_downloaded'),
                'info_size'=>$this->post('info_size'),
                'info_purchase'=>$this->post('info_purchase'),
                'info_dev'=>$this->post('info_dev'),
                'info_release'=>$this->post('info_release'),
                'info_age'=>$this->post('info_age')
            ];

            if ($this->info->createInfo($data) > 0) {
                $this->response([
                    'status' => true,
                    'message'=> 'new Info has been created'
                ], REST_Controller::HTTP_CREATED);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'failed to created new Info!' 
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }

        public function index_put()
        { //mengedit data
            $id = $this->put('info_id');
            $data = [
                'app_id'=> $this->put('app_id'),
                'info_version'=> $this->put('info_version'),
                'info_requirement'=> $this->put('info_requirement'),
                'info_last_update'=> $this->put('info_last_update'),
                'info_downloaded'=> $this->put('info_downloaded'),
                'info_size'=>$this->put('info_size'),
                'info_purchase'=>$this->put('info_purchase'),
                'info_dev'=>$this->put('info_dev'),
                'info_release'=>$this->put('info_release'),
                'info_age'=>$this->put('info_age')
            ];

            if ($this->info->updateInfo($data,$id) > 0) {
                $this->response([
                    'status' => true,
                    'message'=> 'info has been updated'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'failed to update info!' 
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

?>