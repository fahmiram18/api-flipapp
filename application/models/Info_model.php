<?php 
    class info_model extends CI_Model{
        public function getInfo($id = null){
            if($id === null){
                return $this->db->get('info')->result_array();
            }else{
                return $this->db->get_where('info',['info_id'=>$id])->result_array();
            }
        }

        public function deleteInfo($id){
            $this->db->delete('info',['info_id'=> $id]);
            return $this->db->affected_rows();
        }
        public function createInfo($data){
            $this->db->insert('info',$data);
            return $this->db->affected_rows();
        }
        public function updateInfo($data,$id){
            $this->db->update('info',$data,['info_id'=>$id]);
            return $this->db->affected_rows();
        }
    }

?>