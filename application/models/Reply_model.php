<?php 
    class Reply_model extends CI_Model {

        private $table_name = 'replies';

        public function get($comment_id)
        {
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);
            $this->db->select('
                reply_id,
                replies.comment_id,
                replies.user_id,
                users.user_name,
                replies.reply_date,
                replies.reply
            ');
            $this->db->from($this->table_name);
            $this->db->join('comments', 'comments.comment_id = replies.comment_id');
            $this->db->join('users', 'users.user_id = replies.user_id');        
            $this->db->order_by('reply_date', 'desc');
            if ($comment_id != null) {
                $this->db->where('replies.comment_id', $comment_id);
                $result = $this->db->get()->result_array();
            }
            $this->db->trans_complete();
    
            if ($this->db->trans_status()) {
                return $result;
            } else {
                return false;
            }
        }

        public function create($data)
        {
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);
            $this->db->insert($this->table_name,$data);
            $result = $this->db->affected_rows();
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                return $result;
            } else {
                return FALSE;
            }
        }

        public function delete($reply_id){
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);
            $this->db->where('reply_id', $reply_id);
            $this->db->delete($this->table_name);
            $result = $this->db->affected_rows();
            $this->db->trans_complete();
    
            if ($this->db->trans_status()) {
                return $result;
            } else {
                return FALSE;
            }
        }

        public function deleteAll($comment_id){
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);
            $this->db->where('comment_id', $comment_id);
            $this->db->delete($this->table_name);
            $result = $this->db->affected_rows();
            $this->db->trans_complete();
    
            if ($this->db->trans_status()) {
                return $result;
            } else {
                return FALSE;
            }
        }
        
        public function update($reply_id, $data)
        {
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);
            $this->db->where('reply_id', $reply_id);
            $this->db->update($this->table_name,$data);
            $result = $this->db->affected_rows();
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                return $result;
            } else {
                return FALSE;
            }
        }
    }