<?php

class Profile_model extends CI_Model{
	public function getProfile($user_id = null)
	{
		if ($user_id == null) {
		return $this->db->get('user')->result_array();
		} else{
			return $this->db->get_where('user',['user_id'=>$user_id])->result_array()[0];
		}
	}

	public function deleteProfile($user_id)
	{
		$this->db->delete('user',['user_id' => $user_id]);
		return $this->db->affected_rows();
	}


	public function updatePersonal($user_id, $data)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('user',$data);
		return $this->db->affected_rows();

	}

	public function updateCredit($user_id, $credit)
	{
		$this->db->set('credit', $credit);
		$this->db->where('user_id', $user_id);
		$this->db->update('user');
		return $this->db->affected_rows();

	}
	public function updateSpec($data, $user_id)
	{
		$this->db->update('user',$data,['user_id'=> $user_id]);
		return $this->db->affected_rows();

	}

	public function updateAvatar($user_id, $avatar)
	{
		$this->db->set('image', $avatar);
		$this->db->where('user_id', $user_id);
		$this->db->update('user');
		return $this->db->affected_rows();
	}

}