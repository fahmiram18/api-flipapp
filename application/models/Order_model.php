<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function get($id = null)
	{
        $this->db->order_by('ship_date', 'asc');
		$this->db->join('app', 'checkout.app_id = app.app_id');
        if($id == null){
            return $this->db->get('checkout')->result_array();
        }else{
            return $this->db->get_where('checkout', ['checkout_token' => $id])->result_array();
        }
    }

	public function finish($id)
	{
        $this->db->delete('checkout',['checkout_token'=> $id]);
        return $this->db->affected_rows();
    }

    public function search($key){
    	$this->db->join('app', 'checkout.app_id = app.app_id');
    	$this->db->like('app_name', $key, 'BOTH');
    	$this->db->or_like('app_dev', $key, 'BOTH');
    	$this->db->or_like('app_desc', $key, 'BOTH');
    	$this->db->or_like('checkout_token', $key, 'BOTH');
    	$this->db->or_like('username', $key, 'BOTH');
    	$this->db->or_like('email', $key, 'BOTH');
    	$this->db->or_like('address', $key, 'BOTH');
    	$this->db->or_like('ship_method', $key, 'BOTH');
    	$this->db->or_like('ship_date', $key, 'BOTH');
    	$this->db->or_like('phone', $key, 'BOTH');
    	return $this->db->get('checkout')->result_array();
    }

}

/* End of file order_model.php */
/* Location: ./application/models/order_model.php */