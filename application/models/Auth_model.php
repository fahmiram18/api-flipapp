<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    private $table_name = 'users';

    public function get($email = null, $password = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            user_id,
            user_name,
            user_email,
            user_phone,
            user_avatar,
            user_password
        ');
        $this->db->from($this->table_name);
        $this->db->where('user_email', $email);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            if (count($result) == 0) {
                return 0;
            } else {
                return $result[0];
            }

        } else {
            return FALSE;
        }
    }

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }
}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */