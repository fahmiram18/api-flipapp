<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

    private $table_name = 'transactions';

	public function get($user_id)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            transactions_history.checkout_token,
            users.user_name,
            users.user_email,
            shipping_methods.shipping_method,
            shipping_date,
            total_price,
            transactions_status.status,
            last_updated,
            comment,
            apps.*
        ');
        $this->db->from($this->table_name);
        $this->db->join('users', 'users.user_id = transactions.user_id');
        $this->db->join('shipping_methods', 'shipping_methods.shipping_method_id = transactions.shipping_method_id');
        $this->db->join('transactions_status', 'transactions_status.transaction_status_id = transactions.transaction_status_id');
        $this->db->join('transactions_history', 'transactions_history.checkout_token = transactions.checkout_token');
        $this->db->join('apps', 'apps.app_id = transactions_history.app_id');
        $this->db->where('transactions_history.user_id', $user_id);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }
    
    // public function getDetail($token)
	// {
	// 	$this->db->trans_start();
    //     $this->db->trans_strict(FALSE);
    //     $this->db->select('
    //         apps.*
    //     ');
    //     $this->db->from($this->table_name);
    //     $this->db->join('transactions_history', 'transactions_history.checkout_token = transactions.checkout_token');
    //     $this->db->join('apps', 'transactions_history.app_id = apps.app_id');
    //     $this->db->where('transactions_history.checkout_token', $token);
    //     $result = $this->db->get()->result_array();
    //     $this->db->trans_complete();

    //     if ($this->db->trans_status()) {
    //         return $result;
    //     } else {
    //         return FALSE;
    //     }
	// }

	public function create($data)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }
    
    public function _checkedout($user_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('user_id', $user_id);
        $this->db->update('carts', ['cart_status_id' => 2]);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function _addHistory($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert_batch('transactions_history', $data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
        
    }

}

/* End of file Transaction_model.php */
/* Location: ./application/models/Transaction_model.php */