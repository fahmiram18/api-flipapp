<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review_model extends CI_Model {

    private $table_name = 'reviews';

    public function get($app_id = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
			review_id,
            reviews.app_id,
            reviews.user_id,
            users.user_name,
            review_date,
            review_desc,
            review_rating,
            review_like
		');
        $this->db->from($this->table_name);
		$this->db->join('users', 'users.user_id = reviews.user_id');
		$this->db->join('apps', 'apps.app_id = reviews.app_id');
		if ($app_id != null) {
			$this->db->where('reviews.app_id', $app_id);
			$result = $this->db->get()->result_array();
		}
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
    }

    // public function count($id) {
    //     $query = $this->db->get_where('review',['app_id'=>$id]);
    //     return $query->num_rows();
    // }

    public function delete($review_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('review_id', $review_id);
        $this->db->delete($this->table_name);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }
    public function create($data)
    {
        $this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->insert($this->table_name, $data);
		$result = $this->db->affected_rows();
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
    }

    public function update($data, $review_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('review_id', $review_id);
        $this->db->update($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();
    
        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function _average($app_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        // menghitung median rating
        $this->db->select_avg('review_rating');
        $this->db->where('app_id', $app_id);
        $this->db->from('reviews');
        $result = $this->db->get()->result_array()[0]['review_rating'];
        // update rating pada table apps
        $this->db->update('apps', ['app_review_value' => $result]);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();
    
        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

}

/* End of file review_model.php */
/* Location: ./application/models/review_model.php */