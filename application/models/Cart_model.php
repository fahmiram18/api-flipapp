<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_model extends CI_Model {

	private $table_name = 'carts';

	public function get($user_id = null)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->select('
			cart_id,
			carts.app_id,
			apps.app_name,
			apps.app_price,
			apps.app_poster,
			categories.category_name
		');
		$this->db->from($this->table_name);
		$this->db->join('apps', 'apps.app_id = carts.app_id');
		$this->db->join('categories', 'categories.category_id = apps.category_id');
		$this->db->where('cart_status_id', 1);
		if ($user_id != null) {
			$this->db->where('carts.user_id', $user_id);
			$result = $this->db->get()->result_array();
		}
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function _total_price($user_id)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->select('
			sum(apps.app_price) as total_price
		');
		$this->db->from($this->table_name);
		$this->db->join('apps', 'apps.app_id = carts.app_id');
		$this->db->where('cart_status_id', 1);
		if ($user_id != null) {
			$this->db->where('carts.user_id', $user_id);
			$result = $this->db->get()->result_array()[0];
		}
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	// public function getAll()
	// {
	// 	$this->db->trans_start();
	// 	$this->db->trans_strict(FALSE);
	// 	$this->db->select('
	// 		cart_id,
	// 		carts.app_id,
	// 		apps.app_name,
	// 		apps.app_price,
	// 		apps.app_poster,
	// 		categories.category_name
	// 	');
	// 	$this->db->from($this->table_name);
	// 	$this->db->join('app', 'apps.app_id = carts.app_id');
	// 	$this->db->join('category', 'categories.category_id = apps.category_id');
	// 	$result = $this->db->get()->result_array();
	// 	$this->db->trans_complete();

	// 	if ($this->db->trans_status()) {
	// 		return $result;
	// 	} else {
	// 		return FALSE;
	// 	}
	// }

	public function create($data)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->insert($this->table_name, $data);
		$result = $this->db->affected_rows();
		$this->db->trans_complete();
		
		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function delete($cart_id)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('cart_id', $cart_id);
        $this->db->delete($this->table_name);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
	}
}

/* End of file cart_model.php */
/* Location: ./application/models/cart_model.php */