<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer_model extends CI_Model {

	public function get()
	{        
        $this->db->where('role_id', 2);
        $this->db->or_where('role_id', 3);
        return $this->db->get('user')->result_array();
    }

    public function add($data)
    {
        $this->db->insert('user',$data);
        return $this->db->affected_rows();
    }

}

/* End of file developer_model.php */
/* Location: ./application/models/developer_model.php */