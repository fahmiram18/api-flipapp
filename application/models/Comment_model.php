<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends CI_Model {

    private $table_name = 'comments';

    public function get($app_id = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            comment_id,
            comments.app_id,
            comments.user_id,
            users.user_name,
            comments.comment_date,
            comments.comment
        ');
        $this->db->from($this->table_name);
        $this->db->join('apps', 'apps.app_id = comments.app_id');
        $this->db->join('users', 'users.user_id = comments.user_id');        
        $this->db->order_by('comment_date', 'desc');
        if ($app_id != null) {
            $this->db->where('comments.app_id', $app_id);
            $result = $this->db->get()->result_array();
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
    }

    public function update($comment_id, $data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('comment_id', $comment_id);
        $this->db->update($this->table_name,$data);
        $result = $this->db->affected_rows();
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
    }
    
    public function delete($comment_id){
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('comment_id', $comment_id);
        $this->db->delete($this->table_name);
        $result = $this->db->affected_rows();
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
    }

}

/* End of file Comments_model.php */
