<?php

class Portfolio_model extends CI_Model {

	private $table_name = 'apps';
	
	public function get($user_id)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->select('
            users.user_name,
            users.user_email,
			apps.*,
			app_status.app_status
        ');
        $this->db->from($this->table_name);
		$this->db->join('users', 'users.user_id = apps.user_id');
		$this->db->join('app_status', 'app_status.app_status_id = apps.app_status_id');
		
        $this->db->where('apps.user_id', $user_id);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
	}
}