<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model {

	private $table_name = 'users';

	public function get($user_id = null)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
			user_id,
			user_name,
            user_email,
            user_github,
			user_phone,
			user_avatar,
			user_joined,
			user_company,
			user_company_address,
			user_city,
			user_state,
			user_zip,
			user_credit,
			user_spec_1,
			user_spec_2,
			user_spec_3,
			user_about
		');
		
        $this->db->from($this->table_name);
        if ($user_id != null) {
        	$this->db->where('user_id', $user_id);
        	$result = $this->db->get()->result_array()[0];
        } else {
        	$result = FALSE;
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
	}

	public function update($user_id, $data)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('user_id', $user_id);
        $this->db->update($this->table_name, $data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
	}

}

/* End of file Account_model.php */
/* Location: ./application/models/Account_model.php */