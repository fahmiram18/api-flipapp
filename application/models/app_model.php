<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends CI_Model {

    private $table_name = 'apps';

	public function get($app_id = null)
	{
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            app_id,
            app_name,
            categories.category_name,
            users.user_name,
            app_desc,
            app_price,
            app_poster,
            app_screen_capture_1,
            app_screen_capture_2,
            app_screen_capture_3,
            app_version,
            app_released,
            app_review_value,
            app_requirement,
            app_demo_link,
            app_last_updated,
            app_purchased
        ');
        $this->db->from($this->table_name);
        $this->db->where('app_status_id', 1);
        $this->db->join('categories', 'categories.category_id = apps.category_id');
        $this->db->join('users', 'users.user_id = apps.user_id');
        if ($app_id != null) {
            $this->db->where('app_id', $app_id);
            
            $result = $this->db->get()->result_array()[0];
        } else {
            $result = $this->db->get()->result_array();
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
	}

	public function search($key)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            app_id,
            app_name,
            categories.category_name,
            users.user_name,
            app_desc,
            app_price,
            app_poster,
            app_screen_capture_1,
            app_screen_capture_2,
            app_screen_capture_3,
            app_version,
            app_released,
            app_review_value,
            app_requirement,
            app_demo_link,
            app_last_updated,
            app_purchased
        ');
        $this->db->from($this->table_name);
        $this->db->where('app_status_id', 1);
        $this->db->join('categories', 'categories.category_id = apps.category_id');
        $this->db->join('users', 'users.user_id = apps.user_id');
    	$this->db->like('apps.app_name', $key, 'BOTH');
    	$this->db->or_like('users.user_name', $key, 'BOTH');
    	$this->db->or_like('apps.app_desc', $key, 'BOTH');
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function filter($category_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            app_id,
            app_name,
            categories.category_name,
            users.user_name,
            app_desc,
            app_price,
            app_poster,
            app_screen_capture_1,
            app_screen_capture_2,
            app_screen_capture_3,
            app_version,
            app_released,
            app_review_value,
            app_requirement,
            app_demo_link,
            app_last_updated,
            app_purchased
        ');
        $this->db->from($this->table_name);
        $this->db->join('categories', 'categories.category_id = apps.category_id');
        $this->db->join('users', 'users.user_id = apps.user_id');
        $this->db->where('app_status_id', 1);
        $this->db->where_in('apps.category_id', $category_id);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function update($app_id, $data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('app_id', $app_id);
        $this->db->update($this->table_name, $data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function delete($app_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('app_id', $app_id);
        $this->db->delete($this->table_name);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();
        
        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

}

/* End of file App_model.php */
/* Location: ./application/models/App_model.php */