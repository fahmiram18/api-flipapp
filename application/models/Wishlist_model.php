<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist_model extends CI_Model {

	private $table_name = 'wishlists';

	public function get($user_id = null)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->select('
			wishlist_id,
			wishlists.app_id,
			apps.app_name,
			apps.app_desc,
			apps.app_price,
			apps.app_poster,
			categories.category_name
		');
		$this->db->from($this->table_name);
		$this->db->join('apps', 'apps.app_id = wishlists.app_id');
		$this->db->join('categories', 'categories.category_id = apps.category_id');
		if ($user_id != null) {
			$this->db->where('wishlists.user_id', $user_id);
			$result = $this->db->get()->result_array();
		}
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function getAll()
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->select('
			wishlist_id,
			wishlists.app_id,
			apps.app_name,
			apps.app_price,
			apps.app_poster,
			categories.category_name
		');
		$this->db->from($this->table_name);
		$this->db->join('apps', 'apps.app_id = wishlists.app_id');
		$this->db->join('categories', 'categories.category_id = apps.category_id');
		$result = $this->db->get()->result_array();
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function create($data)
	{
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$this->db->where('app_id', $data['app_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get($this->table_name)->result_array();
		if ($result) {
			$result = 'Data already exist!';
		} else {
			$this->db->insert($this->table_name, $data);
			$result = $this->db->affected_rows();		
		}
		$this->db->trans_complete();

		if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function delete($wishlist_id)
	{
		$this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('wishlist_id', $wishlist_id);
        $this->db->delete($this->table_name);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
	}
}

/* End of file Wishlist_model.php */
/* Location: ./application/models/Wishlist_model.php */