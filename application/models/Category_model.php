<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    private $table_name = 'categories';

	public function get($category_id = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        if ($category_id != null) {
            $this->db->where('category_id', $category_id);
            $result = $this->db->get($this->table_name)->result_array()[0];
        } else {
            $result = $this->db->get($this->table_name)->result_array();
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function add($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function delete($category_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->delete($this->table_name,['category_id'=> $category_id]);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

    public function update($category_id, $data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('category_id', $category_id);
        $this->db->update($this->table_name, $data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }

}

/* End of file category_model.php */
/* Location: ./application/models/category_model.php */