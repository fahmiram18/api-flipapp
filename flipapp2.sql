-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Agu 2019 pada 14.32
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flipapp2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `apps`
--

CREATE TABLE `apps` (
  `app_id` int(5) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `app_name` varchar(50) NOT NULL,
  `app_desc` varchar(2000) NOT NULL,
  `app_price` int(20) NOT NULL,
  `app_poster` varchar(255) NOT NULL DEFAULT 'https://amentiferous-grass.000webhostapp.com/assets/img/poster/default.jpg',
  `app_screen_capture_1` varchar(255) NOT NULL DEFAULT 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg',
  `app_screen_capture_2` varchar(255) NOT NULL DEFAULT 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg',
  `app_screen_capture_3` varchar(255) NOT NULL DEFAULT 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg',
  `app_version` varchar(20) NOT NULL,
  `app_released` date NOT NULL DEFAULT current_timestamp(),
  `app_review_value` float NOT NULL DEFAULT 0,
  `app_requirement` text NOT NULL,
  `app_demo_link` varchar(128) NOT NULL,
  `app_last_updated` date NOT NULL DEFAULT current_timestamp(),
  `app_purchased` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `apps`
--

INSERT INTO `apps` (`app_id`, `category_id`, `user_id`, `app_name`, `app_desc`, `app_price`, `app_poster`, `app_screen_capture_1`, `app_screen_capture_2`, `app_screen_capture_3`, `app_version`, `app_released`, `app_review_value`, `app_requirement`, `app_demo_link`, `app_last_updated`, `app_purchased`) VALUES
(1, 1, 1, 'Automatic Lamp', 'Qwertyuiop asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm.', 1000000, 'https://amentiferous-grass.000webhostapp.com/assets/img/poster/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', '1.5.123', '2019-07-17', 0, 'Qwertyuiop asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm.', 'htpps://example.com', '2019-07-17', 0),
(83, 1, 1, 'Automatic Door', 'Qwertyuiop asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm.', 2000000, 'https://amentiferous-grass.000webhostapp.com/assets/img/poster/img_1563423299.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', '1.3.21', '0000-00-00', 0, 'Qwertyuiop asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm.', 'https://www.example.com/', '0000-00-00', 0),
(84, 2, 1, 'E-Learning', 'Qwertyuiop asdfghjl zxcvbnm qwertyuiop.', 1000000, 'https://amentiferous-grass.000webhostapp.com/assets/img/poster/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', 'https://amentiferous-grass.000webhostapp.com/assets/img/screen_capture/default.jpg', '2.3.456', '2019-07-18', 0, 'Qwertyuiop asdfghjl zxcvbnm qwertyuiop.', 'https://www.example.com/', '2019-07-18', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(11) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_updated` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `carts`
--

INSERT INTO `carts` (`cart_id`, `app_id`, `user_id`, `last_updated`) VALUES
(5, 1, 2, '2019-07-25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Smart Home'),
(2, 'Smart School');

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_date` date NOT NULL DEFAULT current_timestamp(),
  `comment` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, 'flip123', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(5) NOT NULL,
  `info_id` int(5) NOT NULL,
  `permission_detail` varchar(100) NOT NULL,
  `category_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `replies`
--

CREATE TABLE `replies` (
  `reply_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply_date` date NOT NULL DEFAULT current_timestamp(),
  `reply` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reviews`
--

CREATE TABLE `reviews` (
  `review_id` int(5) NOT NULL,
  `app_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `review_date` date NOT NULL DEFAULT current_timestamp(),
  `review_desc` varchar(500) NOT NULL,
  `review_rating` int(1) NOT NULL,
  `review_like` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `shipping_methods`
--

CREATE TABLE `shipping_methods` (
  `shipping_method_id` int(11) NOT NULL,
  `shipping_method` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `shipping_methods`
--

INSERT INTO `shipping_methods` (`shipping_method_id`, `shipping_method`) VALUES
(1, 'GitHub'),
(2, 'E-Mail');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `checkout_token` varchar(20) NOT NULL,
  `app_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shipping_method_id` int(11) NOT NULL,
  `shipping_date` date NOT NULL,
  `comment` longtext NOT NULL,
  `transaction_status_id` int(11) NOT NULL,
  `last_updated` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions_status`
--

CREATE TABLE `transactions_status` (
  `transaction_status_id` int(11) NOT NULL,
  `status` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `transactions_status`
--

INSERT INTO `transactions_status` (`transaction_status_id`, `status`) VALUES
(1, 'pending'),
(2, 'complete');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(128) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_github` varchar(64) NOT NULL,
  `user_phone` varchar(16) NOT NULL,
  `user_avatar` varchar(128) NOT NULL DEFAULT 'https://amentiferous-grass.000webhostapp.com/assets/img/avatar/default.jpg',
  `user_password` varchar(256) NOT NULL,
  `user_active_status` int(1) NOT NULL,
  `user_joined` date NOT NULL DEFAULT current_timestamp(),
  `user_company` varchar(30) NOT NULL,
  `user_company_address` varchar(30) NOT NULL,
  `user_city` varchar(30) NOT NULL,
  `user_state` varchar(30) NOT NULL,
  `user_zip` varchar(20) NOT NULL,
  `user_credit` varchar(20) NOT NULL,
  `user_spec_1` varchar(128) NOT NULL,
  `user_spec_2` varchar(128) NOT NULL,
  `user_spec_3` varchar(128) NOT NULL,
  `user_about` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_github`, `user_phone`, `user_avatar`, `user_password`, `user_active_status`, `user_joined`, `user_company`, `user_company_address`, `user_city`, `user_state`, `user_zip`, `user_credit`, `user_spec_1`, `user_spec_2`, `user_spec_3`, `user_about`) VALUES
(1, 'Fahmi Ramadhan', 'muhammadfahmir18@gmail.com', '', '081536606724', 'https://amentiferous-grass.000webhostapp.com/assets/img/avatar/default.jpg', '$2y$10$KFmpsI7qZRVWAZdVRALbx.3KMb7icYoVRCZ7Z/PxyIprPs0PoGP0m', 1, '2019-07-16', 'PROLOGIC', 'Jl. Bukit Raya Selatan no. 172', 'Bandung', 'West Java', '40142', '', '', '', '', ''),
(2, '', '', '', '', 'https://amentiferous-grass.000webhostapp.com/assets/img/avatar/default.jpg', '$2y$10$F6317Hk498Rxm09b8FE/b.VeVgc4gGhV1SWR186Kezfg9MZ5qHuIu', 1, '2019-07-26', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wishlists`
--

CREATE TABLE `wishlists` (
  `wishlist_id` int(11) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_updated` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `wishlists`
--

INSERT INTO `wishlists` (`wishlist_id`, `app_id`, `user_id`, `last_updated`) VALUES
(8, 1, 1, '2019-07-26'),
(9, 84, 1, '2019-07-26');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`app_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indeks untuk tabel `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `wishlist_ibfk_1` (`user_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indeks untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indeks untuk tabel `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`reply_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `shipping_methods`
--
ALTER TABLE `shipping_methods`
  ADD PRIMARY KEY (`shipping_method_id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`checkout_token`),
  ADD KEY `shipping_method_id` (`shipping_method_id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `transaction_status_id` (`transaction_status_id`);

--
-- Indeks untuk tabel `transactions_status`
--
ALTER TABLE `transactions_status`
  ADD PRIMARY KEY (`transaction_status_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`wishlist_id`),
  ADD KEY `wishlist_ibfk_1` (`user_id`),
  ADD KEY `app_id` (`app_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `apps`
--
ALTER TABLE `apps`
  MODIFY `app_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT untuk tabel `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT untuk tabel `replies`
--
ALTER TABLE `replies`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `reviews`
--
ALTER TABLE `reviews`
  MODIFY `review_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT untuk tabel `shipping_methods`
--
ALTER TABLE `shipping_methods`
  MODIFY `shipping_method_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transactions_status`
--
ALTER TABLE `transactions_status`
  MODIFY `transaction_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `apps`
--
ALTER TABLE `apps`
  ADD CONSTRAINT `apps_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `apps_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`);

--
-- Ketidakleluasaan untuk tabel `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `replies_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`comment_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `replies_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Ketidakleluasaan untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`shipping_method_id`) REFERENCES `shipping_methods` (`shipping_method_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`transaction_status_id`) REFERENCES `transactions_status` (`transaction_status_id`);

--
-- Ketidakleluasaan untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  ADD CONSTRAINT `wishlists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlists_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
